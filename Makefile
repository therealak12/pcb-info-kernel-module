CONFIG_MODULE_SIG=n
ccflags-y := -std=gnu99                                                                                                                                                                       
TARGET_MODULE:=pinfo_module

obj-m := $(TARGET_MODULE).o


KERNEL_DIR:=/lib/modules/$(shell uname -r)/build
PWD:=$(shell pwd)

all:
	$(MAKE) -C $(KERNEL_DIR) M=$(PWD) modules
clean:
	$(MAKE) -C $(KERNEL_DIR) M=$(PWD) clean
load:
	sudo insmod $(TARGET_MODULE).ko
unload:
	sudo rmmod $(TARGET_MODULE) || true>/dev/null

