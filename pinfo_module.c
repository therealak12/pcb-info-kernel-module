#include <linux/init.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/slab.h> // for kmalloc
#include <linux/proc_fs.h>
#include <linux/pid.h>
#include <linux/fdtable.h>
#include <linux/time.h>

#include "common.h"

#define PINFO_DEV_NAME "pcb_info"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("therealak12");


static dev_t pinfo_dev;
static struct cdev *pinfo_cdev;

int *given_pid;

// prototypes
/*static int pinfo_open(struct inode *, struct file *);*/
/*static int pinfo_release(struct inode *, struct file *);*/
static ssize_t pinfo_read(struct file *, char *, size_t, loff_t *);
static ssize_t pinfo_write(struct file *, const char *, size_t, loff_t *);

const struct file_operations pinfo_fops = {
    .owner = THIS_MODULE,
    .read = pinfo_read,
    .write = pinfo_write
};

static ssize_t pinfo_write(struct file *filp, const char *buff, size_t count, loff_t *offset) {
    given_pid = (int *)kmalloc(sizeof(int), GFP_KERNEL);
    if (copy_from_user(given_pid, buff, count))
        return -EFAULT;
    printk(KERN_INFO "given pid =   %d", *given_pid);
    return sizeof(buff);
}

static ssize_t pinfo_read(struct file *filp, char *buff, size_t count, loff_t *offset) {
    struct pid *pid_struct = find_get_pid(*given_pid);
    struct task_struct *task = pid_task(pid_struct, PIDTYPE_PID);
    pcb_info result;
    result.state = task->state;
    struct fdtable *files_table = files_fdtable(task->files);
    int file_count = 0;
    while (files_table->fd[file_count] != NULL) {
	    file_count += 1;
    }
    file_count -= 1;
    result.files = (char**)kmalloc(GFP_KERNEL, sizeof(char*) * file_count);
    for (int i = 0; i < file_count; ++i) {
	result.files[i] = kmalloc(GFP_KERNEL, 100 * sizeof(char));
	struct path files_path = files_table->fd[i]->f_path;
	char *tmp;
    	result.files[i] = d_path(&files_path, tmp, 100 * sizeof(char));
    }
    result.nivcsw = task->nivcsw;
    result.nvcsw = task->nvcsw;
    result.start_time = task->start_time;
    result.real_start_time = task->real_start_time;
    copy_to_user(buff, &result, count);
    return 1;
}

static int __init pinfo_init(void) {
    int result = alloc_chrdev_region(&pinfo_dev, 0, 1, PINFO_DEV_NAME);
    if (result < 0) {
        printk(KERN_ALERT "alloc_chrdev_region failed");
        return result;
    }

    pinfo_cdev = cdev_alloc();
    if (pinfo_cdev == NULL) {
        printk(KERN_ALERT "cdev_alloc failed");
        return -1;
    }
    cdev_init(pinfo_cdev, &pinfo_fops);

    result = cdev_add(pinfo_cdev, pinfo_dev, 1);
    if (result < 0) {
        printk(KERN_ALERT "cdev_init or cdev_add failed");
        result = -2;
        cdev_del(pinfo_cdev);
        unregister_chrdev_region(pinfo_dev, 1);
        return result;
    }

    return result;
}

static void __exit pinfo_exit(void) {
    cdev_del(pinfo_cdev);
    unregister_chrdev_region(pinfo_dev, 1);

    printk(KERN_INFO "pcb_info_kernel module exited.");
}

module_init(pinfo_init);
module_exit(pinfo_exit);
