#!/bin/bash

module="pinfo_module"
device="pcb_info"
mode="666"

rmmod $module || true>/dev/null
/sbin/insmod ./$module.ko $* || exit 1

rm -f /dev/${device}

major=$(awk "\$2==\"$device\" {print \$1}" /proc/devices)

mknod /dev/${device} c $major 0

group="staff"
grep -q '^staff:' /etc/group || group="wheel"

chgrp $group /dev/${device}
chmod $mode /dev/${device}


