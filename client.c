#include <fcntl.h> // for file modes like O_RDWR                                                                                                                                              
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> // for open, lseek, read, ...
#include <errno.h> // for error numbers

#include "common.h"

#define DEV_NAME "/dev/pcb_info"

int main(int argc, char *argv[]) {
    int fd = open(DEV_NAME, O_RDWR);
    if (fd < 0) {
        perror("failed to open pcb_info character device");
        return 1;
    }   

    if (argc != 5) {
	    printf("inappropriate args, exiting...");
	    return 0;
    }
    int process_id[1];
    process_id[0] = atoi(argv[4]);
    int interval = atoi(argv[2]);
    while (1) {
	    pcb_info read_buff;
	    long long r_result = write(fd, process_id, sizeof(process_id));
	    r_result = read(fd, &read_buff, sizeof(pcb_info));
	    int file_count = sizeof(read_buff.files) / sizeof(char*);
	    if (file_count > 0) {
		    printf("%d files are opened by this process:\n", file_count);
		    for (int i = 0; i < file_count; ++i) {
			    //printf("%s    ", read_buff.files[i]);
		    }
		    printf("\n");
	    }
	    printf("The process state = %ld\n", read_buff.state);
	    printf("The process nivcws = %lu\n", read_buff.nivcsw);
	    printf("The process nvcsw = %lu\n", read_buff.nvcsw);
	    printf("The process start_time = %llu\n", read_buff.start_time);
	    printf("The process real_start_time = %llu\n", read_buff.real_start_time);
	    sleep(interval);
    }

    close(fd);
    return 0;
}

